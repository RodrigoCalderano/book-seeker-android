# Kotlin-Android-bookSeeker




An MVVM Android project written in [Kotlin](https://kotlinlang.org/) that consumes [iTunesAPI](https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/).

## Libraries
* [Retrofit 2](http://square.github.io/retrofit/)
* [RxJava 2](https://github.com/ReactiveX/RxJava) and [RxAndroid](https://github.com/ReactiveX/RxAndroid)
* [Room](https://developer.android.com/jetpack/androidx/releases/room)
* [Glide](https://github.com/bumptech/glide)
* [Dagger 2](http://google.github.io/dagger/)
* [LifeCycle](https://developer.android.com/reference/android/arch/lifecycle/package-summary)
* [Google Support Libraries](http://developer.android.com/tools/support-library/index.html)
* [Mockito](http://mockito.org/)
* [Anko](https://github.com/Kotlin/anko)
* [AppCenter](https://github.com/microsoft/appcenter-sdk-android)


